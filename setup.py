#!/usr/bin/env python3

from distutils.core import setup

name     = "metagames"
base_url = "http://chadok.info/metagames"

with open("./metagames/version.py") as fp:
    exec(fp.read())

with open('README.txt') as file:
    long_description = file.read()

setup(
    name         = name,
    version      = __version__,
    description  = "Non official client for various video games platforms",
    long_description = long_description,
    author       = "Olivier Schwander",
    author_email = "olivier.schwander@chadok.info",
    url          = base_url,
    download_url = base_url + "/" + name + "-" + __version__ + ".tar.gz",
    packages     = ["metagames", "metagames.website"],
    scripts      = ["scripts/metagames"],
    requires     = ["PyYAML", "requests", "beautifulsoup4"],
    classifiers  = [
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        ],
)
