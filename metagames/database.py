import sqlite3
from datetime import datetime
from pathlib import Path

""" Database structure

Table providers:
- name: string # steam, humble, gog

Table platforms:
- name: string # windows, linux, mac, android, audio, ebook

Table products:
- provider: string      # Foreign key to the table provider
- id: string            # Unique identifier in the provider system
- title: string         # Human-readable name
- name: string          # Machine-readable name
- url: string           # Webpage on the provider website
- meta: string          # Provider-specific metadata
- purchase_date: int    # Purchase date

Table products_platforms:
- provider: string
- id: string
- platform: string

Table downloads:
- provider: string          # Foreign key to the table provider
- id: string                # Foreign key to the table product
- filename: string          # Filename
- description: string       # Human-readable description
- size: int                 # Size in bytes
- md5, sha1, sha256: string # Various hashes
- depends: string           # Foreign key to the table download, name of a needed file
- lang: string              # Language
- installer: bool           # Is it an installer ?
- platform: string          # Foreign key to the table platform
- version: string           # Version number if available
- first_seen: int           # first seen date
- meta: string              # Provider-specific metadata
- obsolote: int             # true if and only if the file is not available anymore on the provider

"""

_enable_foreign_keys = "PRAGMA foreign_keys = ON;"

_create = [
    """CREATE TABLE IF NOT EXISTS providers (
    name TEXT PRIMARY KEY
    );""",

    """CREATE TABLE IF NOT EXISTS platforms (
    name TEXT PRIMARY KEY
    );""",

    # """CREATE TABLE IF NOT EXISTS languages (
    # name TEXT PRIMARY KEY
    # );""",

    """CREATE TABLE IF NOT EXISTS products (
    provider TEXT,
    id TEXT,
    title TEXT,
    name TEXT,
    url TEXT,
    meta TEXT,
    purchase_date INT,
    PRIMARY KEY (provider, id),
    FOREIGN KEY (provider) REFERENCES providers(name)
    );""",

    """CREATE TABLE IF NOT EXISTS products_platforms (
    provider TEXT,
    id TEXT,
    platform TEXT,
    PRIMARY KEY (provider, id, platform),
    FOREIGN KEY (provider) REFERENCES providers(name),
    FOREIGN KEY (platform) REFERENCES platforms(name),
    FOREIGN KEY (id) REFERENCES products(id)
    );""",

    """CREATE TABLE IF NOT EXISTS downloads (
    provider TEXT,
    id TEXT,
    filename TEXT,
    version TEXT,
    first_seen INT,
    description TEXT,
    size INT,
    md5 TEXT,
    sha1 TEXT,
    sha256 TEXT,
    lang TEXT,
    installer INT,
    platform TEXT,
    depends TEXT,
    meta TEXT,
    obsolete INT,
    PRIMARY KEY (provider, id, filename, platform),
    FOREIGN KEY (provider) REFERENCES providers(name),
    FOREIGN KEY (provider, id) REFERENCES products(provider, id),
    FOREIGN KEY (platform) REFERENCES platforms(name),
    FOREIGN KEY (provider, id, depends, platform) REFERENCES downloads(provider, id, filename, platform)
    );""",
    # XXX platform is part of the primary key, since, in some cases, the
    # same file can be presented as available for different platforms
    # (SG_SGS_source_code.zip from Shadowgrounds Survivor). In this
    # case, during an update, with the INSERT OR REPLACE query, the one
    # which will end in the database is not determined.
]

_insert_provider  = "INSERT OR IGNORE INTO providers VALUES (?);"
_insert_platform  = "INSERT OR IGNORE INTO platforms VALUES (?);"
_insert_product   = "INSERT OR IGNORE INTO products VALUES (?,?,?,?,?,?,?);"
_insert_download  = "INSERT OR REPLACE INTO downloads VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"

_select_products  = "SELECT * FROM products ORDER BY %s;"
_select_downloads = """SELECT * FROM downloads AS d, products AS p
WHERE d.id = p.id %s GROUP BY filename, platform ORDER BY d.provider, p.name, d.filename;"""
_select_find_download = """SELECT * FROM downloads AS d, products AS p
WHERE d.id = p.id AND d.provider = ? AND p.name = ? AND d.filename = ? GROUP BY filename;"""
# XX Since platform is part of the primary key, theses queries should
# return different answers for the same file with different platform in
# order to avoid false positive UNKNOWN errors.

_update_downloads_obsolete = "UPDATE downloads SET obsolete = 1 WHERE provider = ? AND id = ?;"

def product2tuple(product):
    return (
        product["provider"],
        product["id"],
        product["title"],
        product["name"],
        product["url"],
        product["meta"],
        product["purchase_date"],
    )

def download2tuple(download):
    return (
        download['provider'],
        download['id'],
        download['filename'],
        download['version'],
        download['first_seen'],
        download['description'],
        download['size'],
        download['md5'],
        download['sha1'],
        download['sha256'],
        download['lang'],
        download['installer'],
        download['platform'],
        download['depends'],
        download['meta'],
        0, # A file inserted this way is never obsolete
    )

class Database:
    def __init__(self, path):
        self._path = path

        if Path(path).is_file():
            self._conn = sqlite3.connect('file:' + path + '?mode=ro', uri=True)
        else:
            self._conn = sqlite3.connect(path)
            self._prepare()

        self._conn.row_factory = sqlite3.Row

    def readwrite(self):
        self._conn = sqlite3.connect(str(self._path))
        self._prepare()

        self._conn.row_factory = sqlite3.Row

    def _prepare(self):
        c = self._conn

        c.execute(_enable_foreign_keys)

        for query in _create:
            c.execute(query)

        for provider in ["steam", "gog", "humble"]:
            c.execute(_insert_provider, (provider,))

        for platform in ["linux", "windows", "mac", "android", "audio", "ebook"]:
            c.execute(_insert_platform, (platform,))

        c.commit()

    def update(self, website):
        c = self._conn

        for product in website.products():
            print("Processing {title} from {provider}".format(**product))
            c.execute(_insert_product, product2tuple(product))

            # XXX Mark all downloads from this product as obsolote
            c.execute(_update_downloads_obsolete, (product["provider"], product["id"]))

            for download in website.downloads(product):
                c.execute(_insert_download, download2tuple(download))

            c.commit()

    def products(self, order='alpha'):
        c = self._conn

        allowed_order = {
            'alpha': "title",
            'ralpha': "title DESC",
            'date': "purchase_date",
            'rdate': "purchase_date DESC",
        }
        order = allowed_order.get(order, allowed_order['alpha'])

        return c.execute(_select_products % order)

    def downloads(self, exclude={}, provider=None, obsolete=False):
        c = self._conn

        if "platforms" in exclude and len(exclude["platforms"]) > 0:
            platforms = ["d.platform != '%s'" % x for x in exclude["platforms"]]
            platforms = "AND (" + " AND ".join(platforms) + ")\n"
        else:
            platforms = ""

        if "games" in exclude and len(exclude["games"]) > 0:
            games = ["p.name != '%s'" % x for x in exclude["games"]]
            games = "AND (" + " AND ".join(games) + ")\n"
        else:
            games = ""

        if "files" in exclude and len(exclude["files"]) > 0:
            files = ["d.filename NOT GLOB '%s'" % x for x in exclude["files"]]
            files = "AND (" + " AND ".join(files) + ")\n"
        else:
            files = ""

        if provider is None:
            provider_filter = ""
        else:
            provider_filter = "AND d.provider ='%s'\n" % provider

        obsolete_filter = "AND obsolete = {}\n".format(int(obsolete))

        filters = platforms + games + files + provider_filter + obsolete_filter
        select = _select_downloads % filters

        return c.execute(select)

    def find_download(self, provider, name, filename):
        c = self._conn

        return list(c.execute(_select_find_download, (provider, name, filename)))[0]

