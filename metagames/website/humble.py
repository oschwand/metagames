from os.path import basename
import json
from datetime import datetime
import time
from urllib.parse import urlparse
import json

from bs4 import BeautifulSoup
import requests

from .website import *

def is_installer(platform, filename):
    if platform == "audio":
        return False
    if platform == "ebook":
        return False
    else:
        return True

class Humble(Website):
    name    = "humble"
    website = "https://www.humblebundle.com"

    _url_login         = "https://www.humblebundle.com/login"
    _url_processlogin  = "https://www.humblebundle.com/processlogin"
    _url_humbleguard   = "https://www.humblebundle.com/user/humbleguard"
    _url_account       = "https://www.humblebundle.com/home/library"
    _url_order_details = "https://www.humblebundle.com/api/v1/order/"
    _url_bundle        = "https://www.humblebundle.com/downloads?key="

    def login_start(self, email, password):
        login = self._session.get(self._url_login)
        content = login.text
        csrf_begin = content.find("value='", content.find("csrftoken")) + len("value='")
        csrf_end   = content.find("'", csrf_begin)
        csrf = content[csrf_begin:csrf_end]

        payload = {
            "_le_csrf_token": csrf,
            "authy-token"   : "",
            "goto"          : "/home",
            "password"      : password,
            "qs"            : "",
            "submit-data"   : "",
            "username"      : email,
        }

        login = self._session.post(self._url_processlogin, data=payload)

        try:
            answer = login.json()
            # print(answer)
        except ValueError:
            raise LoginFailure(login.status_code)
        if not answer["success"]:
            if "username" in answer["errors"]:
                raise LoginUsernameUnknown(answer["errors"]["username"][0])
            if "captcha" in answer["errors"]:
                raise LoginCaptcha(answer["errors"]["captcha"][0])
            raise LoginPasswordFailure(answer["errors"])

        home = self._session.get(self._base + login.json()["goto"])
        if home.text.find("Account Protection"):
            raise LoginTwoFactor()

    def login_verify(self, code):
        payload = {
            "code": code,
            "goto": "",
            "qs"  : "",
        }

        verify = self._session.post(self._url_humbleguard, data=payload)

        try:
            if not verify.json()["success"]:
                raise LoginTwoFactorFailed(verify.json()["errors"]["code"][0])
        except ValueError:
            if verify.status_code != 200:
                raise LoginTwoFactorFailed(verify.status_code)

    def login_end(self):
        self._session.cookies.save()

    def initialize(self):
        self._gamekeys  = self._params.get("keys", [])
        self._gamekeys += self._get_gamekeys()

    def _get_gamekeys(self):
        home = self._session.get(self._url_account)
        home = BeautifulSoup(home.text, "lxml")
        user_home_json_data = home.find("script", id="user-home-json-data")
        user_home_json_data = json.loads(user_home_json_data.text)
        return user_home_json_data["gamekeys"]

    def products(self):
        self._downloads = {}

        for key in self._gamekeys:
            r = self._session.get(self._url_order_details + key)
            assert(r.status_code == 200)
            data = r.json()
            purchase_date = int(datetime.strptime(data["created"], "%Y-%m-%dT%H:%M:%S.%f").timestamp())

            for product in data["subproducts"]:
                item = {}
                item["provider"]      = self.name
                item["id"]            = product["machine_name"] # XXX
                item["title"]         = product["human_name"]
                item["name"]          = product["machine_name"]
                item["url"]           = product["url"]
                item["meta"]          = "{}"
                item["purchase_date"] = purchase_date

                self._downloads[item["id"]] = (key, product["downloads"])

                # remove useless ZERO WIDTH SPACE characters
                item["title"] = item["title"].translate({
                    ord('\u200b'): None
                })

                yield item

    def downloads(self, product):
        gamekey, downloads = self._downloads[product["id"]]

        for download in downloads:
            platform = download["platform"]

            for file in download["download_struct"]:
                if "asm_config" in file:
                    continue

                if "url" not in file or "web" not in file["url"]:
                    continue

                url = urlparse(file["url"]["web"])
                filename = basename(url.path)

                item = {}
                item['provider']    = self.name
                item['id']          = product["id"]
                item['filename']    = filename
                item['version']     = file.get("timestamp", None) # XXX Don't know if it is really pertinent
                item['first_seen']  = int(time.time())
                item['description'] = file["name"]
                item['size']        = file.get("file_size", file.get("human_size", None))
                item['md5']         = file.get("md5", None)
                item['sha1']        = file.get("sha1", None)
                item['sha256']      = None
                item['lang']        = None
                item['installer']   = is_installer(platform, filename)
                item['platform']    = platform
                item['depends']     = None
                item['meta']        = json.dumps({'gamekey': gamekey})

                yield(item)

    def find_download_url(self, download):
        gamekey = json.loads(download["meta"])["gamekey"]
        r = self._session.get(self._url_order_details + gamekey)
        assert(r.status_code == 200)
        data = r.json()

        for product in data["subproducts"]:
            if product["machine_name"] == download["name"]:
                break

        for dl in product["downloads"]:
            for file in dl["download_struct"]:
                if "url" not in file:
                    continue

                http_url = file["url"]["web"]
                url = urlparse(http_url)
                filename = basename(url.path)

                if filename == download["filename"]:
                    return http_url

if __name__ == "__main__":
    import os.path

    cookies = os.path.expanduser("~/.cache/metagames")
    website = Humble(cookies)
    if website.is_logged():
        website.initialize()
        for product in website.products():
            print(product)
    else:
        print("Not logged in", website.website)
        website.login_start(input("Username:"), input("Password:"))
        website.login_end()

