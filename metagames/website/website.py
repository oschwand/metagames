import os.path
from http.cookiejar import MozillaCookieJar, Cookie

import requests

class LoginFailure(Exception):
    pass
class LoginTwoFactor(LoginFailure):
    pass
class LoginTwoFactorFailed(LoginFailure):
    pass
class LoginCaptcha(LoginFailure):
    pass
class LoginPasswordFailure(LoginFailure):
    pass
class LoginUsernameUnknown(LoginFailure):
    pass
class LoginUsernameInvalid(LoginFailure):
    pass

def set_cookies(driver, cookiejar):
    for cookie in driver.get_cookies():
        # https://github.com/python/cpython/blob/3.8/Lib/http/cookiejar.py#L736
        ck = Cookie(None,
                    cookie["name"],
                    cookie["value"],
                    cookie.get("port", None),
                    cookie.get("port", None) is not None,
                    cookie["domain"],
                    True,
                    cookie["domain"][0] == ".",
                    cookie['path'],
                    True,
                    cookie["secure"],
                    cookie.get('expiry', None),
                    False,
                    None,
                    None,
                    False,
                    None
        )
        cookiejar.set_cookie(ck)

class Website:
    name    = None
    website = None

    def __init__(self, path_cookies, **params):
        self._params = params

        cookies = MozillaCookieJar(os.path.join(path_cookies, self.name + ".txt"))

        self._session = requests.Session()
        self._session.cookies = cookies

        try:
            self._session.cookies.load()
        except FileNotFoundError:
            pass

    def login_start(self, username, password):
        raise NotImplementedError

    def login_twofactor(self, code):
        raise NotImplementedError

    def login_end(self):
        self._session.cookies.save()

    def is_logged(self):
        r = self._session.get(self._url_account, allow_redirects=True)

        if r.status_code == 200:
            return True
        if r.status_code == 302:
            return False

    def initialize(self):
        pass

    def products(self):
        raise NotImplementedError

    def downloads(self):
        raise NotImplementedError

    def find_download_url(self, download):
        raise NotImplementedError
