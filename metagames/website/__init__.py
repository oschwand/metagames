from .website import *
from .gog import Gog
from .humble import Humble

Websites = {
    "gog": Gog,
    "humble": Humble,
}

