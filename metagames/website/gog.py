#! /usr/bin/python3

from os.path import basename, splitext
from urllib.parse import urlparse, parse_qs
import re
import time
import json

from bs4 import BeautifulSoup

from .website import *

def url2filename(url):
    url   = urlparse(url)
    query = parse_qs(url.query)
    path  = query["path"][0]
    return basename(path)

languages = {
    "English": "en",
    "français": "fr",
    "italiano": "it",
    "magyar": "hu",
    "polski": "pl",
    "русский": "ru",
    "中文": "cn",
    "中文(简体)": "cn",
    "español": "es",
    "Español (AL)": "es-al",
    "Deutsch": "de",
    "český": "cz",
    "日本語": "jp",
    "한국어": "ko",
    "Türkçe": "tr",
    "português": "pt",
    "Suomi": "fi",
    "suomi": "fi",
    "română": "ro",
    "nederlands": "nl",
    "български": "bl",
    "Português do Brasil": "br",
    "Ελληνικά": "gr",
    "yкраїнська": "uk",
    "Dansk": "da",
    "norsk": "no",
    "svenska": "sv",
    "slovenský": "sk",
    "العربية": "ar",
}

def is_installer(platform, filename):
    if platform == "windows":
        return filename.startswith("setup") and filename.endswith(".exe")
    elif platform == "linux":
        return filename.startswith("gog") and filename.endswith(".sh")
    elif platform == "mac":
        return filename.endswith(".dmg")

def get_dependencies(description, filename):
    if filename.endswith("bin"):
        return []

    m = re.match(".*\(\d+\s+\w+\s+(\d+)\)$", description)
    if m:
        total = int(m.group(1))
        name, _ = splitext(filename)
        return [ name + "-" + str(i) + ".bin" for i in range(1, total) ]
    else:
        return []

class Gog(Website):
    name    = "gog"
    website = "https://www.gog.com"

    _url_account      = "https://www.gog.com/account"
    _url_games        = "https://www.gog.com/account/getFilteredProducts?hasHiddenProducts=false&hiddenFlag=0&isUpdated=0&mediaType=1&sortBy=date_purchased&system=&page={}"
    _url_game_details = "https://www.gog.com/account/gameDetails/{}.json"

    def login_start(self, username, password):
        #     raise LoginEmailUnknown(email)
        #     raise LoginUsernameInvalid(email)
        #     raise LoginPasswordFailure()

        # late importe to avoid failure if selenium is missing and if the
        # login has been done elsewhere
        import selenium
        from selenium.webdriver.common.keys import Keys
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as EC
        from selenium.webdriver.common.by import By

        driver = selenium.webdriver.Chrome()
        driver.get(self._url_account)

        WebDriverWait(driver, 100).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="GalaxyAccountsFrame"]'))
        )

        frame = driver.find_element_by_xpath('//*[@id="GalaxyAccountsFrame"]')
        driver.switch_to.frame(frame)

        input_username = driver.find_element_by_id("login_username")
        input_username.clear()
        input_username.send_keys(username)
        input_username.send_keys(Keys.TAB)

        error = driver.find_element_by_xpath('//*[@id="error_login_username"]')
        if error.is_displayed():
            print("Login failure:", error.text)
            driver.close()
            raise LoginUsernameInvalid(username)

        input_password = driver.find_element_by_id("login_password")
        input_password.clear()
        input_password.send_keys(password)

        captcha = len(driver.find_elements_by_xpath('//iframe[starts-with(@src, "https://www.google.com/recaptcha/api2/anchor")]')) > 0
        if captcha:
            input("Captcha detected, please validate manually then press Enter *here*...")

        input_login = driver.find_element_by_id("login_login")
        input_login.send_keys(Keys.RETURN)

        try:
            errors = driver.find_elements_by_xpath('//span[@class="field__msg"]')
            if len(errors) > 0:
                print("Login failure:", errors[0].get_attribute('innerHTML'))
                driver.close()
                raise LoginPasswordFailure()
        except selenium.common.exceptions.NoSuchWindowException:
            pass

        driver.get(self._url_account)

        if driver.current_url == self._url_account:
            set_cookies(driver, self._session.cookies)
            driver.close()
        else:
            driver.close()
            raise LoginFailure()

    def is_logged(self):
        r = self._session.get(self._url_account, allow_redirects=False)

        if r.status_code == 200:
            return True
        if r.status_code == 302:
            return False

    def products(self):
        page = 1
        while True:
            r = self._session.get(self._url_games.format(page))
            assert(r.status_code == 200)
            data = r.json()

            for product in data["products"]:
                item = {}
                item["provider"]      = self.name
                item["id"]            = product["id"]
                item["title"]         = product["title"]
                item["name"]          = product["slug"]
                item["url"]           = self.website + product["url"]
                item["meta"]          = "{}"
                item["purchase_date"] = - int(time.time())
                # XXX Purchase date not directly available.
                #
                # However, the current date coupled with a purchase date
                # ordered search should be enough to store the purchase
                # order.
                #
                # Minus the current date because the date ordered search
                # begins with the most recent purchase

                yield item

            page += 1
            if page > data["totalPages"]:
                break

    def downloads(self, product):
        r = self._session.get(self._url_game_details.format(product["id"]))
        assert(r.status_code == 200)
        data = r.json()

        dependencies = {}

        for lang, downloads in data["downloads"]:
            language = languages[lang]

            for platform in downloads:
                for dl in downloads[platform]:
                    url  = self.website + "/" + dl["manualUrl"]
                    r = self._session.head(url, allow_redirects=True)
                    # r.status_code == 429 seems to be normal

                    filename = url2filename(r.url) # ??? So the HEAD is only useful to get the filename after all the redirections ?

                    for dep in get_dependencies(dl["name"], filename):
                        dependencies[dep] = filename

                    item = {}
                    item['provider']    = self.name
                    item['id']          = product["id"]
                    item['filename']    = filename
                    item['version']     = dl["version"]
                    item['first_seen']  = int(time.time())
                    item['description'] = dl["name"]
                    item['size']        = None # r.headers['Content-Length'] is always 1661
                    item['md5']         = None
                    item['sha1']        = None
                    item['sha256']      = None
                    item['lang']        = language
                    item['installer']   = is_installer(platform, filename)
                    item['platform']    = platform
                    item['depends']     = dependencies.get(filename, None)
                    item['meta']        = json.dumps({'url': url})

                    yield(item)

    def find_download_url(self, download):
        url = json.loads(download["meta"])["url"]
        r = self._session.head(url, allow_redirects=True)
        # r.status_code == 429 seems to be normal

        return r.url

if __name__ == "__main__":
    import os.path

    cookies = os.path.expanduser("~/.cache/metagames")
    website = Gog(cookies)
    if website.is_logged():
        website.initialize()
        for product in website.products():
            print(product)
    else:
        print("Not logged in", website.website)
        humble.login_start(input("Username:"), input("Password:"))
        humble.login_end()
