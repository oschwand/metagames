import os
from os.path import dirname, getsize, isfile
import io
import hashlib
import subprocess

def xdg_config_dir():
    config_home = os.getenv("XDG_CONFIG_HOME",
                            os.path.expanduser("~/.config/"))
    return os.path.join(config_home, "metagames")

def xdg_cache_dir():
    config_home = os.getenv("XDG_CACHE_HOME",
                            os.path.expanduser("~/.cache/"))
    return os.path.join(config_home, "metagames")

def makedirs(path):
    try:
        os.makedirs(path)
    except OSError:
        pass

def compute_file_hash(path, hasher):
    buf_size = io.DEFAULT_BUFFER_SIZE
    
    with open(path, "rb") as file:
        buf = file.read(buf_size)
        hasher.update(buf)
        while buf:
            buf = file.read(buf_size)
            hasher.update(buf)

    return hasher.hexdigest()

def check_file_md5(path, expected):
    hash = compute_file_hash(path, hashlib.md5())
    # print("Computed:", hash, "Expected:", expected)
    return hash == expected

def check_file_sha1(path, expected):
    hash = compute_file_hash(path, hashlib.sha1())
    # print("Computed:", hash, "Expected:", expected)
    return hash == expected

def check_file_size(path, expected):
    size = os.path.getsize(path)
    return size == expected

def wget(url, dest, size=None, dryrun=False):
    if size is not None:
        if isfile(dest) and size == getsize(dest):
            print("Nothing to do for", dest)
            return

    cmdline = ["wget", "-c", "-O", dest, url]

    assert dest is not None
    assert url is not None

    if dryrun:
        print(" ".join('"' + str(x) + '"' for x in cmdline))
    else:
        makedirs(dirname(dest))
        subprocess.call(cmdline)

